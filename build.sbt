name := "simple-vocabulary-teacher"

organization := "pl.jborkowski"

version := "1.0"

scalaVersion := "2.11.8"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

routesGenerator := InjectedRoutesGenerator

com.typesafe.sbt.SbtScalariform.scalariformSettings

routesImport += "binders.PathBinders._"

routesImport += "binders.QueryBinders._"

libraryDependencies += filters